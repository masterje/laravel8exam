<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DriveController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/test', [DriveController::class, 'index']);
Route::get('/get-records', [DriveController::class, 'getRecords']);
Route::post('/delete', [DriveController::class, 'deleteRecord']);
Route::post('/save', [DriveController::class, 'save']);
Route::post('/check', [DriveController::class, 'checkRecord']);
Route::post('/return-car', [DriveController::class, 'updateRecord']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
