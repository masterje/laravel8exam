<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drives', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('first_name');
            $table->string('last_name');
            $table->bigInteger('phone');
            $table->string('email');
            $table->string('make');
            $table->string('plate'); //random plate number
            $table->enum('transaction', ['loan', 'test'])->default('test'); //transaction type if loan or test drive
            $table->date('checkout_date');
            $table->date('return_date');
            $table->boolean('is_out')->default(0); // is checked out
            $table->boolean('is_returned')->default(0);
            $table->enum('condition', ['good', 'bad'])->default('good');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drives');
    }
}
