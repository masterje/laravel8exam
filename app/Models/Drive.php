<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Drive extends Model
{
    use HasFactory;

    protected $table = 'drives';

    protected $fillable = [
      'first_name',
      'last_name',
      'phone',
      'email',
      'make',
      'plate',
      'checkout_date',
      'return_date',
      'is_out',
      'is_returned',
      'transaction'
    ];
}
