<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
      $carsAPI = Http::get('https://vpic.nhtsa.dot.gov/api/vehicles/GetMakesForVehicleType/car?format=json');
      $response = $carsAPI->json();

      $cars = [];
      if(isset($response['Results']))
      {
        $cars = $response['Results'];
      }

//echo '<pre>'; print_r($cars); exit;

      return view('welcome',['cars' => $cars]);
    }
}
