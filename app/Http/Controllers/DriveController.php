<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Drive;
class DriveController extends Controller
{
    //just a check
    public function index()
    {
      return 'ok';
    }

    public function save(Request $request)
    {
      $check = $this->check( $request->input('email'), $request->input('plate') );
      if($check)
      {
        return ['error' => 'vehicle not available'];
      }

      return Drive::create($request->all());
    }

    private function check($email, $plate)
    {
      $drive = new Drive;
      $result = $drive->where('plate', $plate)->where('email',$email) ->first();

      return $result;
    }

    public function getRecords()
    {
      $drive = new Drive;
      $result = $drive->all();

      return $result;
    }

    public function checkRecord(Request $request)
    {
      $result = $this->check( $request->input('email'), $request->input('plate') );
      $data = ['error' => "no record found"];

      if($result)
      {
        $data = $result;
      }

      return $data;
    }

    public function updateRecord(Request $request)
    {
      $result = $this->check( $request->input('email'), $request->input('plate') );

      if($result)
      {
        //we can just use the updated_at field for date and time of return
        $drive = new Drive;
        $record = $drive->find($result->id);
        $record->is_returned = 1 ; //we just set this to one since the car is being is_returned
	$record->condition = $request->input('condition');
	$record->is_out = 0; 
        $record->save();

        return $record;
      }
    }

    public function deleteRecord(Request $request)
    {
        //we can just use the updated_at field for date and time returned
        $drive = new Drive;
        $record = $drive->find($request->input('id'));

        if($record)
        {
          $record->delete();

          return ['message' => 'ok'];
        }

        return ['message' => 'not found'];

    }

}
